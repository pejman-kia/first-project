import http from "./httpservice";

import config from "./config.json"

export const loginuser = user => {
    return http.post(
        `${config.localapi}/api/login`,
        JSON.stringify(user)
    );
};

export const loginUser = user => {
    return http.post(`${config.toplearnapi}/api/login`, JSON.stringify(user));
};
