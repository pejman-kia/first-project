
import React from 'react';
import Login from './component/login/Login';

import { BrowserRouter,Route,Routes} from "react-router-dom"

function App() {
  return (
   
    
    <BrowserRouter>
  <Routes>

      <Route path="/login" element={<Login/>}/>
        
  </Routes>
    </BrowserRouter>
    
    
     
    
  );
}

export default App;
