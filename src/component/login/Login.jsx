import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Row, Col, Button, Container, Alert, Nav } from "react-bootstrap";
import "./StyleLog.css";
import { ToastContainer, toast } from "react-toastify";

import { loginuser } from "../../services/userservice";

function Login() {
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");

  const reset = () => {
    setemail("");
    setpassword("");
  };

  const handlesubmit = async (event) => {
    event.preventDefault();
    const users = {
      email: email,
      password: password,
    };
    try {
      const { status, data } = await loginuser(users);
      if (status === 200) {
        toast.success("welcom");
      }
      console.log(data);
      reset();
    } catch (ex) {
      toast.error("not sent");
      console.log(ex);
    }
  };

  return (
    <Container fluid>
      <Row>

{/* left page */}

        <Col md={6} className="Left">
          <h4 className="danger">DTG</h4>
          <h2>Write a title here for this page</h2>
          <p className="mt-5 mb-5 pwelcom">
            Welcome back! Please login to your account.
          </p>

          <Form  onSubmit={handlesubmit}>
            <fieldset>
              <Form.Group as={Row} className="mb-5">
                <Form.Label as="legend" column md={12}>
                  Admin/Employeer
                </Form.Label>
                <Col sm={8}>
                  <Form.Check
                    type="radio"
                    inline
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                    className="check1"
                  />
                  <Form.Check
                    type="radio"
                    inline
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                    className="check2"
                  />
                </Col>
              </Form.Group>
            </fieldset>


  {/* input email*/}


            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formHorizontalEmail"
            >
              <Col sm={10}>
                <Form.Control
                  type="email"
                  placeholder="Email Address"
                  value={email}
                  onChange={(e) => setemail(e.target.value)}
                />
              </Col>
            </Form.Group>

{/* input password */}

            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formHorizontalPassword"
            >
              <Col sm={10}>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setpassword(e.target.value)}
                />
              </Col>
            </Form.Group>
            

            <Form.Group
              as={Row}
              className="mb-5"
              controlId="formHorizontalCheck"
            >
              <Col sm={{ span: 7, offset: -1 }}>
                <Form.Check label="Remember me" />
              </Col>
              <Col>
                <Alert.Link
                  href="#"
                  className="alertlin"
                >
                  Forgot Password?
                </Alert.Link>
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3">
              <Col sm={{ span: 8, offset: 1 }} className="d-grid gap-2">
                <Button type="submit">Sign in</Button>
              </Col>
            </Form.Group>
          </Form>
        </Col>


{/* right page */}


        <Col md={6}  className="Right">
          <Nav
            variant="pills"
            defaultActiveKey="#home"
            as="ul"
            className="nav"
          >
            <Nav.Item as="li" className="itemHome">
              <Nav.Link href="#home">Home</Nav.Link>
            </Nav.Item>
            <Nav.Item as="li">
              <Nav.Link href="#shopify" className="itemShopify">shopify</Nav.Link>
            </Nav.Item>
          </Nav>
        </Col>
      </Row>
      <ToastContainer />
    </Container>
  );
}

export default Login;
